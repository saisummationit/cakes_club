function AddBannerForm(){
    $.ajax({
        type:'get',
        url:'/admin/banner/add',
        success:function(data){
            $(".bannersView").html(data);
            var message = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Success!</strong> Banner Added successfully.</div>';
            $(".alert-messages").html(message);
        },
        error:function(data){
            var message = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error!</strong> Something went wrong, please try again.</div>';
            $(".alert-messages").html(message);
        }
    });
}

function MakeActive(status,id){
  $.ajax({
    type:'get',
    url:'/admin/banner/changeStatus',
    data:{'status':status,'id':id},
    success:function(data){
      $("#banners-body").html(data);
      var message = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Success!</strong> Status Changed successfully.</div>';
      $(".alert-messages").html(message);
    },
    error:function(data){
      var message = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error!</strong> Something went wrong, please try again.</div>';
      $(".alert-messages").html(message);
    }
  });
}

function MakeDefault(status,id){
  $.ajax({
    type:'get',
    url:'/admin/banner/changeDefault',
    data:{'status':status,'id':id},
    success:function(data){
      if(data == 1){
        var message = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error!</strong> Default Banner UnChanged </div>';
      }else{
        $("#banners-body").html(data);
        var message = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Success!</strong> Default Banner Changed successfully.</div>';
      }
      $(".alert-messages").html(message);
    },
    error:function(data){
      var message = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error!</strong> Something went wrong, please try again.</div>';
      $(".alert-messages").html(message);
    }
  });
}

function Delete(id,isdefault){
  if(isdefault == 0){
    var result = confirm("The banner selected is the first banner. If you try to delete, System will automatically picks another banner as first banner.");
    if(!result){
        return false;
    }
  }
  var deleted = confirm("Are you sure to delete this banner.");
  if(!deleted){
    return false;
  }
  $.ajax({
    type:'get',
    url:'/admin/banner/delete',
    data:{'id':id},
    success:function(data){
      if(data == 0){
        var message = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error!</strong> Banner Not Deleted </div>';
      }else{
        $("#banners-body").html(data);
        var message = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Success!</strong> Banner Deleted successfully.</div>';
      }



      $(".alert-messages").html(message);
    },
    error:function(data){
      var message = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error!</strong> Something went wrong, please try again.</div>';
      $(".alert-messages").html(message);
    }
  });
}

function View(id){
  $.ajax({
    type:'get',
    url:'/admin/banner/view',
    data:{'id':id},
    success:function(data){
      $(".bannersView").html(data);
    },error:function(data){
      var message = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error!</strong> Something went wrong, please try again.</div>';
      $(".alert-messages").html(message);
    }
  });
}

function EditBanner(id){
  $.ajax({
    type:"get",
    url:"/admin/banner/edit",
    data:{'id':id},
    success:function(data){
      $(".bannersView").html(data);
    },error:function(data){
      var message = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error!</strong> Something went wrong, please try again.</div>';
      $(".alert-messages").html(message);
    }
  })
}
