function preview_image(event)
{
  var reader = new FileReader();
  reader.onload = function()
  {
    var output = document.getElementById('output_image_default');
    output.src = reader.result;
  }
  reader.readAsDataURL(event.target.files[0]);
}



$(document).on("click","#btnSelectCategory",function(){
debugger;
var selectedvalue = $(".sui-treeview-item-selected").find("b").data('target');
var selectedcategory = $(".sui-treeview-item-selected").find("b").data('text');
$("#selectedCategory").val(selectedcategory);
$("#categoryId").val(selectedvalue);
$("#selectCategoryModal").modal('hide');
});

function AddProduct(){
  $.ajax({
    type:'get',
    url:'/admin/product/add',
    success:function(data){
      $(".productsView").html(data);
    },
    error:function(data){
      var message = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error!</strong> Something went wrong, please try again.</div>';
      $(".alert-messages").html(message);
    }
  });
}
function editProduct(id){
  debugger;
  $.ajax({
    type:"get",
    url:'/admin/product/edit',
    data:{'id':id},
    success:function(data){
      $(".productsView").html(data);
    },error:function(data){
      var message = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error!</strong> Something went wrong, please try again.</div>';
      $(".alert-messages").html(message);
    }
  })
}
function DeleteProduct(id){
  debugger;
  var deleted = confirm("Are you sure to delete this product.");
  if(!deleted){
    return false;
  }
  $.ajax({
    type:'get',
    url:'/admin/product/delete',
    data: {"id": id,    },
    success:function(data){
      if(data == 0){
        var message = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error!</strong> Product Not Deleted.</div>';
      }else{
        $("#products-body").html(data);
        var message = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Success!</strong> Product Deleted successfully.</div>';
      }
      $(".alert-messages").html(message);
    },
    error:function(data){
      var message = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error!</strong> Something went wrong, please try again.</div>';
      $(".alert-messages").html(message);
    }
  });
}
function displayProduct(view,id){
  debugger;
  var viewStatus;
  if(view == 0){
      viewStatus = 1;
      var toBeView = confirm("Are you sure to make this product to hide on customer page.");
  }else{
      viewStatus = 0;
      var toBeView = confirm("Are you sure to make this product to visible on customer page.");
  }
  var appendColumn = $('#viewIcon' +id);
  if(!toBeView){
    return false;
  }
    $.ajax({
      type:'get',
      url:'/admin/product/toBeViewOnCustomer',
      data:{'view':viewStatus,'id':id},
      success:function(data){

        if(data == 1){
          var message = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error!</strong> Product view on customer page UnChanged </div>';
        }else{
          $("#products-body").html(data);
          var message = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Success!</strong> Product view on customer page Changed successfully.</div>';
        }
        $(".alert-messages").html(message);
      },
      error:function(data){
        var message = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error!</strong> Something went wrong, please try again.</div>';
        $(".alert-messages").html(message);
      }
    });
}
function addProductsList(){
  debugger;
  var products = [];
  var producttext = [];
            $.each($("input[name='product']:checked"), function(){
                products.push($(this).val());
                producttext.push($(this).next('label').text());
            });
      products.join(", ");
      producttext.join(",");
      $('#relatedprdList').val(products);
      $('#relatedprd').val(producttext);
      $("#selectProductsModal").modal('hide');
}
