@extends('client.master')
@section('content')
<div class="container payment-details" style="margin-top:-650px;">
<div class="row">
<div id="accordion" style="width:68%">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed checkout-page" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
         Cart Summary
        </button>
      </h5>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed checkout-page" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
         Adress and Contact Details 
        </button>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
      <form>
              <div class="row">
                <div class="col">
                  <input type="text" class="form-control form-control-sm" placeholder="First name">
                </div>
                <div class="col">
                  <input type="text" class="form-control form-control-sm" placeholder="Last name">
                </div>
              </div><br>
              <div class="row">
                <div class="col">
                  <input type="number" class="form-control form-control-sm" placeholder="mobile number">
                </div>
                <div class="col">
                  <input type="number" class="form-control form-control-sm" placeholder="alternate number">
                </div>
              </div><br>
              <div class="row">
                <div class="col">
                  <input type="text" class="form-control form-control-sm" placeholder="Building,Flat number">
                </div>
                <div class="col">
                  <input type="text" class="form-control form-control-sm" placeholder="address">
                </div>
              </div><br>
              <div class="row">
                <div class="col">
                  <input type="text" class="form-control form-control-sm" placeholder="Landmark">
                </div>
                <div class="col">
                  <input type="text" class="form-control form-control-sm" placeholder="City">
                </div>
              </div><br>
              <div class="row">
                <div class="col">
                  <input type="text" class="form-control form-control-sm" placeholder="Pin Code">
                </div>
                <div class="col">
                  <input type="text" class="form-control form-control-sm" placeholder="State">
                </div>
              </div><br>
              <div class="row">
                <div class="col">
                <a href="/checkout" class="btn btn-xs btn1"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;Add New Address</a>
                </div>
                <div class="col">
                <button type="button" class="btn btn1 btn-secondary" style="float:right;">Submit</button>
                </div>
              </div>
        </form>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed checkout-page" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
         Payment Options
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
                <div class="form-check">
            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
            <label class="form-check-label" for="exampleRadios1">
              Debit Card
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
            <label class="form-check-label" for="exampleRadios2">
             Cash On Deliver(COD)
            </label>
          </div>
          <button type="submit" class="btn btn1 btn-secondary" style="float:right;">Pay Now</button>
      </div>
    </div>
  </div>
</div>
    <div class="" style="width:31%;margin-left:1%;">
    <div class="card" style="height:400px;">
        <h5 class="card-header">Price Details</h5>
        <table class="table">
          <tbody>
            <tr>
              <td>Price:</td>
              <td></td>
              <td><i class="fa fa-inr" aria-hidden="true"></i>10000</td>
            </tr>
            <tr>
              <td>Deliver Charges:</td>
              <td></td>
              <td><i class="fa fa-inr" aria-hidden="true"></i>40</td>
            </tr>
            <tr>
              <td>Amout Payble:</td>
              <td></td>
              <td><i class="fa fa-inr" aria-hidden="true"></i>1040</td>
            </tr>
          </tbody>
      </table>
  </div>
</div>
</div>
</div>
</div>
<!-- <br>
<br>
<br> -->
@endsection