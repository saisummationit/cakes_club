@extends('client.master')
@section('content')
<div class="container checkout-div" style="margin-top:-650px;">
		<h4>Items in Your Cart:</h4>
		<table class="table table-striped table-borderless table-responsive-sm table-style">
			<tbody>
				<tr>
					<th scope="row" class="cart-items">
						<div class="row">
							<div class="" style="width:25%;">
								<img src="images/banner3.jpg" height="50" width="50" class="rounded">
							</div>
							<div class="" style="width:70%">
								<span class="cart-text">Chocolate Cake with Chocolate Cream Topping (0.5 Kg) </span>
							</div>
						</div>
					</th>
					<td class="cart-items">&#x20B9;200</td>
					<td class="cart-items">1</td>
					<td ><button type="button" class="btn btn-sm" style="border-radius:50%;"><i class="fa fa-times-circle-o fa-lg" style="color:#ed4027 !important" aria-hidden="true"></i></button></td>
				</tr>
				<!-- <tr>
					<th scope="row" class="cart-items">
						<div class="row">
							<div class="" style="width:25%;">
								<img src="images/banner1.jpg" height="50" width="50" class="rounded">
							</div>
							<div class="" style="width:70%">
								<span class="cart-text">Square Shaped Vanilla Personalised Photo Cake (1 Kg)</span>
							</div>
						</div>
					</th>
					<td class="cart-items">&#x20B9;400</td>
					<td class="cart-items">1</td>
					<td><button type="button" class="btn btn-sm" style="border-radius:50%;"><i class="fa fa-times-circle-o fa-lg" style="color:#ed4027 !important" aria-hidden="true"></i></button></td>
				</tr> -->
				<!-- <tr>
					<th scope="row"class="cart-items">
						<div class="row">
							<div class="" style="width:25%;">
								<img src="images/banner2.jpg" height="50" width="50" class="rounded">
							</div>
							<div class="" style="width:70%">
								<span class="cart-text">Pineapple Cake with Cherry & Cream Toppings (0.5 Kg) </span>
							</div>
						</div>
					</th>
					<td class="cart-items">&#x20B9;800</td>
					<td class="cart-items">1</td>
					<td><button type="button" class="btn btn-sm" style="border-radius:50%;"><i class="fa fa-times-circle-o fa-lg" style="color:#ed4027 !important" aria-hidden="true"></i></button></td>
				</tr> -->
				<td></td>
				<td></td>
				<td></td>
				<th scope="row" class="cart-items" style="text-align:right;">Total : &#x20B9;1600</td>
					</tr>
					<tr>
						<th colspan="4" scope="row" style="text-align:right;"><a href="/checkout"><button type="button" class="btn btn1 btn-secondary">Proceed to Checkout</button></a>
					</tr>

					</tfoot>

		</table>
	</div>
	<div class="container">
		<h6><b> Related Items that you may like</b></h6>
		<div class="row">
			<div id="owl-demo">
				<div class="item"><img class="imgOwl" src="images/banner1.jpg" alt="Owl Image"></div>
				<div class="item"><img class="imgOwl" src="images/banner2.jpg" alt="Owl Image"></div>
				<div class="item"><img class="imgOwl" src="images/banner3.jpg" alt="Owl Image"></div>
				<div class="item"><img class="imgOwl" src="images/banner1.jpg" alt="Owl Image"></div>
				<div class="item"><img class="imgOwl" src="images/banner2.jpg" alt="Owl Image"></div>
				<div class="item"><img class="imgOwl" src="images/banner3.jpg" alt="Owl Image"></div>
			</div>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.js"></script>

		</div>
	</div>
@endsection

