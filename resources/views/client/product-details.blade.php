@extends('client.master')
@section('content')
<div class="container main-div" style="margin-top: -620px; z-index:0;">
        <div class="row">

<!-------------------------------------------------------------------
                     PRODUCT IMAGE
--------------------------------------------------------------------->
        <div id="prod-image-div" class="col-lg-5 col-sm-12">

            <!-- <div class="col-lg-3"></div> -->
        <!-- <div class="col-lg-6"> -->
            <div class="slider-for slide-display">
                <div><img class="img1" src="{{asset('storage/products/'.$product->image_path)}}" alt=""></div>
                @foreach($relatedImages as $image)
                <div><img class="img1" src="{{asset('storage/products/'.$image->image_path)}}" alt=""></div>
                @endforeach
            </div>
            <br>
            <div class="slider-nav" style="height:80px;">
            <div><img class="img1" src="{{asset('storage/products/'.$product->image_path)}}" alt=""></div>
                @foreach($relatedImages as $image)
                <div><img class="img1" src="{{asset('storage/products/'.$image->image_path)}}" alt=""></div>
                @endforeach
            </div>

        <!-- </div> -->
        <!-- <div class="col-lg-3"></div> -->

            <!-- <img class="" src="C:\Users\sum\Desktop\cakes_image.JPG" alt="" style=" width: 100%;"> -->
        </div> <!--PRODUCT IMAGE ENDS HERE-->


<!-------------------------------------------------------------------
                     PRODUCT DESCRIPTION
--------------------------------------------------------------------->
        <div id="prod-deatils" class="col-lg-7 col-sm-12">
            <h5>{{$product->name}}</h5>
            <span class="" style="float:left;">
                <i class="fa fa-inr" style="font-size:18px">&nbsp;<span style="font-size:20px">{{$product->price}}</span> </i>
            </span>
            <br>
            <div class="rating" style="float: left;">
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star"></span>
                <span class="fa fa-star"></span> &emsp;
                <span class="">| <a href="#">Read all 3141 reviews</a></span>
            </div>
            <br>
            <hr>
            <dl>
                <dt>Description</dt>
                <dd>
                    <p>
                        {!! $product->description !!}
                    </p>
                </dd>
            </dl>
            <hr>

            <dl>
                <dt>Product Code</dt>
                <dd>{{$product->code}}</dd>
            </dl>
            <!-- <dl>
                <dt>Color</dt>
                <dd>Black and white</dd>
            </dl> -->
            <!-- <dl>
                <hr>
                <dt>Delivery</dt>
                <form action="" class="form-inline">
                    <div class="form-group">
                        <input type="text" class="form-control" id="usr" name="pinCode"
                            placeholder="enter your pincode">
                    </div>
                    <button type="submit" class="btn btn-xs btn1">Check</button>
                </form>
            </dl> -->
            <hr>
<!-------------------------------------------------------------------
                     QUANTITY AND SIZE
--------------------------------------------------------------------->
<form action="/addtoCart/{{$product->id}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <dl>
                        <dt>Quantity: </dt>
                        <dd>
                            <select class="form-control form-control" id="quantity" style="width:70px;">
                                <option> 1 </option>
                                <option> 2 </option>
                                <option> 3 </option>
                            </select>
                        </dd>
                    </dl>
                </div>
                <div class="col-sm-12 col-md-6">
                    <dl>
                        <dt>Size: </dt>
                        <dd>
                        <label class="radio-inline">
                            <input type="radio" name="optradio" checked> 1kg
                        </label>
                        <label class="radio-inline">
                             <input type="radio" name="optradio">1.5kg
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="optradio">2kg
                        </label>
                        </dd>
                    </dl>
                </div> <!-- SIZE ENDS HERE -->
                <hr>
            </div> <!-- ROW ENDS HERE -->

<!-------------------------------------------------------------------
                    BUTTONS
--------------------------------------------------------------------->
<div class="row"style="float:right;">
                <a href="/addtoCart" class="btn btn-xs btn1"><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp; Buy
                    Now</a>
               <button class="btn btn-xs btn1" type="submit"> <i class="fa fa-cart-plus" aria-hidden="true"></i>&nbsp; Add to
                    cart</button>
            <!-- BUTTONS ENDS HERE -->
</div>
</form>
        </div> <!-- PRODUCT DEATILS  ENDS HERE -->
        </div>
        <br>
         <h6><b> Related Items that you may like</b></h6>
        <div class="row">
                <div id="owl-demo">
                    @foreach($relatedProducts as $product)
                        <div class="item"><img class="imgOwl" src="{{asset('storage/products/'.$product->image_path)}}" alt="Owl Image"></div>


                    @endforeach

                </div>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.js"></script>

        </div>
    </div> <!-- CONATINER ENDS HERE -->
<br>
@endsection
<script>
// 	function addtoCart(){
//   $.ajax({
//     type:'post',
//     url:'/client/Cakes/addtoCart',
//     data:{'quantity':$('#quantity').val(),'id':id},
//     success:function(data){
//       $("#banners-body").html(data);
//       var message = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Success!</strong> Status Changed successfully.</div>';
//       $(".alert-messages").html(message);
//     },
//     error:function(data){
//       var message = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error!</strong> Something went wrong, please try again.</div>';
//       $(".alert-messages").html(message);
//     }
//   });
// }
</script>
