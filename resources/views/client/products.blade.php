@extends('client.master')
@section('carousel-content')
  <!-- Indicators -->
  <div  id="banner-carousel" class="carousel slide container" data-ride="carousel" style="margin-top:15px;">
        <ul class="carousel-indicators">
            <?php $i = 0; ?>
            @foreach($banners as $banner)
                @if($banner->default == 0)
                <li data-target="#banner-carousel" data-slide-to="{{$i}}" class="active"></li>
                @else
                <li data-target="#banner-carousel" data-slide-to="{{$i}}"></li>
                @endif
                <?php $i++; ?>
            @endforeach
        </ul>

        <!-- The slideshow -->
        <div class="carousel-inner" style="height:450px;">
        @foreach($banners as $banner)
            @if($banner->default == 0)
            <div class="carousel-item active">
                <img src="{{asset('storage/banners/'.$banner->imageName)}}" alt="" width="100%" height="100%">
            </div>
            @else
            <div class="carousel-item">
                <img src="{{asset('storage/banners/'.$banner->imageName)}}" alt="" width="100%" height="100%">
            </div>
            @endif

        @endforeach
        </div>

        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#banner-carousel" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#banner-carousel" data-slide="next">
            <span class="carousel-control-next-icon"></span>
        </a>
</div>
<br>
@endsection
@section('content')
<div class="our-webcoderskull padding-lg container" style="margin-top:-680px;" >
<div class="container">
            <ul class="row">
                 @foreach ($products as $product)
                <li class="col-sm-6 col-lg-3">
                    <a href="/getProductDetails/{{$product->id}}">
                        <div class="card cnt-block " style="margin-top:10px;">
                            <img class="card-img-top" src="{{asset('storage/products/'.$product->image_path)}}" alt="Card image cap" style="">
                            <div class="card-body"><?php $out = strlen($product->name) > 55 ? substr($product->name,0,55)."..." : $product->name; ?>
                                <p class="card-text " style="text-align:justify; height:30px;"><b>{{$out}}</b></p>
                                <div class="rating" style="float: left;">
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star unchecked"></span>
                                    <span class="fa fa-star unchecked"></span>
                                    <br>
                                    <span class="" style="float:left;">
                                        <i class="fa fa-inr unchecked" style="font-size:18px">&nbsp;<span
                                                style="font-size:20px">{{$product->price}}</span> </i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                @endforeach
 
            </ul>
        </div>
</div>
<br>
@endsection
<script>
    function grtProductDetails(id){
        alert(id);
    }
</script>