<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="../css/style.css">
    <!-- <link rel="stylesheet" href="../css/details.css"> -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.css" rel="stylesheet" />

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="../css/slick/slick.css" />

        <link rel="stylesheet" type="text/css"
            href="../css/slick/slick-theme.css" />
        <script type="text/javascript" src="../css/slick/slick.min.js"></script>

    <link rel="stylesheet" href="https://use.typekit.net/ylv4gxh.css">
    <link rel="stylesheet" href="https://use.typekit.net/gkr6zpo.css">
    <title>Cakes Club</title>
    <script>
        $(document).ready(function () {

            $("#owl-demo").owlCarousel({
                autoPlay: 3000, //Set AutoPlay to 3 seconds
                items: 4,
                itemsDesktop: [1199, 3],
                itemsDesktopSmall: [979, 3]
            });

        });

        $(document).ready(function () {
            $('.slider-for').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: '.slider-nav'
            });
            $('.slider-nav').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                asNavFor: '.slider-for',
                dots: true,
                centerMode: true,
                focusOnSelect: true
            });
        });

    </script>
</head>

<body>
    <!--------------------------------------------------------------------
                  TOP BAR
-------------------------------------------------------------------->
    <div class="container-fluid main  top-bar" style="font-size:12px !important;">
        <div class="row" align="center">
            <div class="col-6">
                <h6 id="head1" style="font-size:12px;padding:5px;padding-top:10px;">877.BERRIES <a href="#"
                        style="text-decoration:none; color: #A88B61;" color>•&nbsp;COVERED
                        STRAWBERRIES &nbsp; •&nbsp;CHOCOLATE
                        GIFTS </h6></a>
            </div>
            <div class="col-6" style="font-size:12px;padding:5px;padding-top:10px;">
                <span>&nbsp;&nbsp;&nbsp;&nbsp; 24/7 CUSTOMER SERVICE •
                    <span style="font-weight:bold; color:#ED4027">877.237.7437</span>
            </div>
        </div>
    </div>
    <!-- ------------------------------------------------------------------
                 NAVIGATION BAR AND SEARCH
------------------------------------------------------------------ -->
    <div class="container main-navbar">
        <nav class=" navbar navbar-expand-lg navbar-light "
            style="background-color:white;height:auto;box-shadow:0px 0px 0px rgba(0, 0, 0, 0.1);margin-bottom:0px;">
            <a class="navbar-brand" href="#"><img src="../images/berries-logo.png" alt="Cakes Club"
                    style="height:60px;"></a>
            <button class="navbar-toggler faIcon" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <!-- <a class="nav-link" href="#">Cart &nbsp;<i class="fa fa-cart-plus faIcon"
                                aria-hidden="true"></i></a> -->
                        <button type="button" class="btn" data-toggle="modal" data-target="#sem-reg">cart<i class="fa fa-cart-plus faIcon"
                                        aria-hidden="true"></i></button>
                            <div class="modal fade seminor-login-modal" data-backdrop="static" id="sem-reg">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <!-- Modal body -->
                                        <div class="modal-body seminor-login-modal-body">
                                            <!-- <h5 class="modal-title text-center">CREATE AN ACCOUNT</h5> -->
                                            <button type="button" class="close" data-dismiss="modal">
                                                <span><i class="fa fa-times-circle" aria-hidden="true"></i></span>
                                            </button>
                                            <div class="main">
                                                <div class="container">
                                                    <div class="sign-up-content">
                                                    <div class="row">
                                                        <div class="col-sm-12 text-center">
                                                            <button id="login" class="btn btn1 btn-md " Style=" float:left;" OnClick="" >Login</button>
                                                            <button id="register" class="btn btn1 btn-md " Style="float:right;" OnClick="" >Register</button>
                                                        </div>
                                                    </div>
                                                            <br>
                                                        <!-- Login Form -->
                                                        <form method="POST" class="login-form" id="login-form" style="">
                                                        <div class="form-textbox">
                                                                <label>Email</label>
                                                                <input type="email-login" name="email-login" id="email-login" />
                                                            </div>

                                                            <div class="form-textbox">
                                                                <label>Password</label>
                                                                <input type="password" name="pass" id="pass" />
                                                            </div>
                                                                <br>
                                                            <div class="form-textbox">
                                                                <input type="submit" name="submit-login" id="submit-login" class="submit btn1" value="Login" />
                                                            </div>
                                                        </form>
                                                        <!-- Sign-up form -->
                                                        <form method="POST" class="signup-form" id="signup-form" style="display:none;">
                                                       
                                                            <div class="form-textbox">
                                                                <label>Full name</label>
                                                                <input type="text" name="name" id="name" />
                                                            </div>

                                                            <div class="form-textbox">
                                                                <label>Email</label>
                                                                <input type="email-register" name="email-register" id="email-register" />
                                                            </div>

                                                            <div class="form-textbox">
                                                                <label>Password</label>
                                                                <input type="password" name="password" id="password" />
                                                            </div>

                                                            <div class="form-textbox">
                                                                <label>Confirm Password</label>
                                                                <input type="password" name="cnf-password" id="cnf-password" />
                                                            </div>

                                                            <input class="checkbox" type="checkbox" required name="terms" style="display: inline-block;"> <a href="#" style="text-decoration:none;">I, agree the terms and condition</a> 

                                                            <div class="form-textbox">
                                                                <input type="submit" name="submit-register" id="submit-register" class="submit btn1" value="Create Account" />
                                                            </div>
                                                        </form>
                                                        
                                                </div>
                                            </div>
                                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"></a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link disabled" href="#"></a>
                    </li>
                    <li class="nav-item dropdown" style="color: #53301B;">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                            style="color: #212529;">Account &nbsp;
                            <i class="fa fa-user faIcon" aria-hidden="true"></i>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Sign In</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Create Acount</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Track Orders</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Manage Acount</a>
                        </div>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn my-2 my-sm-0" type="submit" style="background-color:#F3ECD3;">Search</button>
                </form>
            </div>
        </nav>
    </div>
    <div class="container" id="occasion-menu">
        <div class="dropdown">
            <span>&emsp;EASTER</span>
            <div class="dropdown-content">
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
            </div>
        </div>
        &emsp;<span class="occasions-bar" style="color:red;">|</span>&emsp;

        <div class="dropdown">
            <span>BIRTHDAY</span>
            <div class="dropdown-content">
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
            </div>
        </div>
        &emsp;<span class="occasions-bar" style="color:red;">|</span>&emsp;
        <div class="dropdown">
            <span>OCCASIONSS</span>
            <div class="dropdown-content">
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
            </div>
        </div>
        &emsp;<span class="occasions-bar" style="color:red;">|</span>&emsp;
        <div class="dropdown">
            <span>FOR HER </span>
            <div class="dropdown-content">
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
            </div>
        </div>
        &emsp;<span class="occasions-bar" style="color:red;">|</span>&emsp;
        <div class="dropdown">
            <span>BERRIES</span>
            <div class="dropdown-content">
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
            </div>
        </div>
        &emsp;<span class="occasions-bar" style="color:red;">|</span>&emsp;
        <div class="dropdown">
            <span>BAKERY </span>
            <div class="dropdown-content">
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
            </div>
        </div>
        &emsp;<span class="occasions-bar" style="color:red;">|</span>&emsp;
        <div class="dropdown">
            <span>CHOCOLATE </span>
            <div class="dropdown-content">
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
            </div>
        </div>
        &emsp;<span class="occasions-bar" style="color:red;">|</span>&emsp;
        <div class="dropdown">
            <span>BASKETS </span>
            <div class="dropdown-content">
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
            </div>
        </div>
        &emsp;<span class="occasions-bar" style="color:red;">|</span>&emsp;
        <div class="dropdown">
            <span>SALE </span>
            <div class="dropdown-content">
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
            </div>
        </div>
        &emsp;<span class="occasions-bar" style="color:red;">|</span>&emsp;
        <div class="dropdown">
            <span>BUSINESS GIFTS </span>
            <div class="dropdown-content">
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
                <a href="#">Best Sellers</a><br>
            </div>
        </div>
        &emsp;<span class="occasions-bar" style="color:red;">|</span>&emsp;
    </div>
    <!-----------------------------------------------------------------------------------
                                      BANNER CAROUSEL
            ------------------------------------------------------------------------------------->
    <div>
      @yield('carousel-content')
    </div>
    <!-----------------------------------------------------------------------------------
                                      PRODUCTS GRID AND IMAGES
            ------------------------------------------------------------------------------------->

    <!-----------------------------------------------------------------------------------
                                       SIDE NAV BAR
            ------------------------------------------------------------------------------------->
    <div class="conatiner-fluid wrapper smaller-screens" style="visibility:hidden;margin-top:0px;">

        <nav id="sidebar" class="col-1" style="z-index:1;">
            <ul class="list-unstyled components">
                <li class="">
                    <a href="#birthdayMenu" data-toggle="collapse" aria-expanded="false"
                        class="dropdown-toggle">BIRTHDAY</a>
                    <ul class="collapse list-unstyled" id="birthdayMenu">
                        <li>
                            <a href="#">BESTSELLERS</a>
                        </li>
                        <li>
                            <a href="#">BIRTHDAY CAKES</a>
                        </li>
                        <li>
                            <a href="#">BIRTHDAY BASKETS</a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="#occasionMenu" data-toggle="collapse" aria-expanded="false"
                        class="dropdown-toggle">OCCASIONS</a>
                    <ul class="collapse list-unstyled" id="occasionMenu">
                        <li>
                            <a href="#">SPRING COLLECTION</a>
                        </li>
                        <li>
                            <a href="#">ST. PATRICK'S DAY GIFTS</a>
                        </li>
                        <li>
                            <a href="#">BIRTHDAY BASKETS</a>
                        </li>
                        <li>
                            <a href="#">ANNIVERSARY</a>
                        </li>
                        <li>
                            <a href="#">BABY SHOWER</a>
                        </li>
                        <li>
                            <a href="#">BIRTHDAY</a>
                        </li>
                        <li>
                            <a href="#">BUSINESS GIFTS</a>
                        </li>
                        <li>
                            <a href="#">CONGRATULATIONS</a>
                        </li>
                        <li>
                            <a href="#">GET WELL SOON</a>
                        </li>
                        <li>
                            <a href="#"> I'M SORRY</a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="#forHerMenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">FOR
                        HER</a>
                    <ul class="collapse list-unstyled" id="forHerMenu">
                        <li>
                            <a href="#">STRAWBERRIES</a>
                        </li>
                        <li>
                            <a href="#">CHERRIES</a>
                        </li>
                        <li>
                            <a href="#">CHOCOLATES</a>
                        </li>

                        <li>
                            <a href="#">CHEESECAKE</a>
                        </li>
                        <li>
                            <a href="#">GIFT BASKETS</a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="#berriesMenu" data-toggle="collapse" aria-expanded="false"
                        class="dropdown-toggle">BERRIES</a>
                    <ul class="collapse list-unstyled" id="berriesMenu">
                        <li>
                            <a href="#">STRAWBERRIES</a>
                        </li>
                        <li>
                            <a href="#">BEST SELLERS</a>
                        </li>
                        <li>
                            <a href="#">BELGIAN CHOCOLATe</a>
                        </li>

                        <li>
                            <a href="#">NO SUGAR ADDED</a>
                        </li>
                        <li>
                            <a href="#">SALTED CARMEL</a>
                        </li>
                        <li>
                            <a href="#">THEMED BY OCCASION</a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="#bakeryMenu" data-toggle="collapse" aria-expanded="false"
                        class="dropdown-toggle">BAKERY</a>
                    <ul class="collapse list-unstyled" id="bakeryMenu">
                        <li>
                            <a href="#">FEATURED</a>
                        </li>
                        <li>
                            <a href="#">BEST SELLERS</a>
                        </li>
                        <li>
                            <a href="#">VIEW ALL</a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="#chocolateMenu" data-toggle="collapse" aria-expanded="false"
                        class="dropdown-toggle">CHOCOLATE</a>
                    <ul class="collapse list-unstyled" id="chocolateMenu">
                        <li>
                            <a href="#">TOP CHOCOLATE GIFTS</a>
                        </li>
                        <li>
                            <a href="#">CHOCOLATE TRUFFLES</a>
                        </li>
                        <li>
                            <a href="#">BEST SELLERS</a>
                        </li>

                        <li>
                            <a href="#">CHOCOLATE BOXES</a>
                        </li>
                        <li>
                            <a href="#">CHOCOLATE GIFT BASKETS</a>
                        </li>
                        <li>
                            <a href="#">COOKIES, PRETZELS & MORE</a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="#basketsMenu" data-toggle="collapse" aria-expanded="false"
                        class="dropdown-toggle">BASKETS</a>
                    <ul class="collapse list-unstyled" id="basketsMenu">
                        <li>
                            <a href="#"> BEST SELLERS</a>
                        </li>
                        <li>
                            <a href="#"> BIRTHDAY BASKETS</a>
                        </li>
                        <li>
                            <a href="#">SAVORY BASKETS</a>
                        </li>

                        <li>
                            <a href="#">VIEW ALL</a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="#" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">SALE</a>
                </li>
                <li class="">
                    <a href="#" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">BUSINESS GIFTS</a>
                </li>
                <li class="">
                    <a href="#accountMenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i
                            class="fa fa-user" aria-hidden="true"></i>&nbsp;ACCOUNTS</a>
                    <ul class="collapse list-unstyled" id="accountMenu">
                        <li>
                            <a href="#">SIGN IN</a>
                        </li>
                        <li>
                            <a href="#">CREATE ACCOUNT</a>
                        </li>
                        <li>
                            <a href="#">TRACK ORDERS</a>
                        </li>

                        <li>
                            <a href="#">MANAGE ACCOUNT</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="list-unstyled CTAs">
                <li>
                    <a hreF="#">24/7 CUSTOMER SERVICE <br>
                        <i class="fa fa-mobile" aria-hidden="true"></i>&nbsp;877.237.7437</a>
                </li>
            </ul>
        </nav>
        <!-----------------------------------------------------------------------------------
                                      MOBILE NAV BAR
            ------------------------------------------------------------------------------------->
        <div id="content" style="margin-top:0px">
            <nav class="navbar navbar-expand-lg navbar-light col-12" id="head-nav" style="z-index:1">
                <div class="container-fluid">
                    <div>
                        <button type="button" id="sidebarCollapse" class="btn btn-sm nav-buttons">
                            <i class="fa fa-align-left"></i>
                        </button>&nbsp; <p class="icon-label">MENU</p>
                    </div>

                    <div>
                        <button type="button" id="btnSearch" class="btn btn-sm nav-buttons" data-toggle="modal"
                            data-target="#myModal">
                            <i class="fa fa-search"></i>
                        </button>&nbsp; <p class="icon-label">SEARCH</p>
                    </div>

                    <img src="images/berries-logo.png" alt=""
                        style="width: 5.5rem;height: 4.2rem;  margin-left: auto; margin-right: auto; ">
                    <div>
                        <button type="button" id="btnCart" class="btn btn-sm nav-buttons">
                            <i class="fa fa-cart-plus"></i>
                        </button>&nbsp; <p class="icon-label" style="">CART</p>
                    </div>
                </div>
            </nav>

        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>


<!---------------------------------------------------------------------------------------------------------------------
                                                 PRODUCT IMAGES
------------------------------------------------------------------------------------------------------------------------>

    <div>
        @yield('content')
    </div>
    <br>
    <br>


    <!-----------------------------------------------------------------------------------
                                       MOBILE-FOOTER
            ------------------------------------------------------------------------------------->
    <div class="container-fluid footer-mobile" style="display: none;">
        <div class="" data-toggle="collapse" data-target="#demo">MORE GIFTS
            <div style="float:right;"><i class="fa fa-plus-square" aria-hidden="true"></i></div>
        </div>
        <hr>
        <div id="demo" class="collapse">
            <li style="list-style-type:none;">
                <a href="#" style="color: #53301B; font-size: 12px;text-decoration: none;">LINK 1</a>
                <div style="float:right">
                    <i class="fa fa-chevron-right"></i>
                </div>
            </li>
            <hr>

            <li style="list-style-type:none;">
                <a href="#" style="color: #53301B; font-size: 12px; text-decoration: none;">LINK 2</a>
                <div style="float:right">
                    <i class="fa fa-chevron-right"></i>
                </div>
            </li>
            <hr>
            <li style="list-style-type:none;">
                <a href="#" style="color: #53301B; font-size: 12px;text-decoration: none;">LINK 3</a>
                <div style="float:right">
                    <i class="fa fa-chevron-right"></i>
                </div>
            </li>
        </div>
    </div>

    <!-----------------------------------------------------------------------------------
                                      MAIN FOOTER
            ------------------------------------------------------------------------------------->
    <div class="container-fluid footer" id="footer-main" style="display:block;">
    <div class="container" style="">
        <div class="row" id="footer-row" style="line-height:2rem">
            <!-- <div class="col-2"></div> -->
            <div class="" style="width:23%;">
                <h6>MORE GIFTS</h6>
                <hr>
                <div id="">
                    <a href="#">BEST SELLERS</a> <br>
                    <a href="#">DIPPED FRUITS</a><br>
                    <a href="#">BAKERY</a><br>
                    <a href="#">SALE</a><br>
                </div>
            </div>

            <div class=""  style="width:23%;padding-left:2%;">
                <h6>OUR COMPANY</h6>
                <hr>
                <div id="">
                    <a href="#">ABOUT GIFTS</a><br>
                    <a href="#">ABOUT OUR COMPANY</a><br>
                    <a href="#">CAREERS</a><br>
                    <a href="#">SITE MAP</a><br>
                </div>
            </div>

            <div class=""  style="width:23%;padding-left:2%;">
                <h6>ACCOUNT INFO</h6>
                <hr>
                <div id="">
                    <a href="#">MANAGE YOUR ACCOUNT</a><br>
                    <a href="#">TRACK YOUR ORDERS</a><br>
                    <a href="#">ORDER HITORY</a><br>
                    <a href="#">CUSTOMER SERVICE</a><br>
                </div>
            </div>
            <div class=""  style="width:25%;padding-left:2%;">
                <h6>SIGN UP</h6>
                <hr>
                <div class="row" style="padding-left:2%;">
                    <div class="col-12" style="background-color: #CEC5A8 ;">
                        <a href="#"><i class="fa fa-book" aria-hidden="true"></i>&nbsp;SIGN UP FOR UPDATES</a>
                    </div>
                </div>
                <br>
                <div class="row" style="padding-left:2%;">
                    <div class="col-12" style="background-color: #CEC5A8 ; ">
                        <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;SIGN UP FOR UPDATES
                    </div>
                </div>
            </div>
            <hr>
        </div>

    </div>
    <!-- <div class="col-1"></div> -->

    </div>
    </div>

<script>
$(document).ready(function() {
  $("#register").click(function() {
    $("#signup-form").show();
    $("#login-form").hide();
  });
  $("#login").click(function() {
    $("#login-form").show();
    $("#signup-form").hide();
  });
});;
</script>
</body>

</html>
