  <br><br>
 @if(count($products) <= 0)
 <center><h3>No Products added</h3></center>
 @else
 <table id="example" class="display table-striped" style="width:100%">
   <thead>
       <tr>
           <th>Image</th>
           <th>Name</th>
           <th>Price</th>
           <th>Category</th>
           <th>Quantity</th>
           <th>Migration</th>
           <th>code</th>
       </tr>
   </thead>
   <tbody id="products-body">
     @foreach ($products as $product)
       <tr>
         <td  style="text-align:center;"><img class="image img-responsive" height="50" width="50" src="{{asset('storage/products/'.$product->image_path)}}"/></td>
         <td style="width:300px; word-wrap:break-word;">{{$product->name}}</td>
         <td>{{$product->price}}</td>
         <td>{{$product->category->name}}</td>
         <td>{{$product->stock}}</td>
         <td>
         @if($product->makeView == 0)
           <button class="btn btn-sm" type="button" data-toggle="tooltip" onClick="displayProduct({{$product->makeView}},{{$product->id}})" data-placement="bottom" title="To be displayed on customer page"><i id="viewIcon{{$product->id}}" class="fa fa-eye"></i></button>
         @else
         <button class="btn btn-sm" type="button" data-toggle="tooltip" onClick="displayProduct({{$product->makeView}},{{$product->id}})" data-placement="bottom" title="To be displayed on customer page"><i id="viewIcon{{$product->id}}" class="fa fa-eye-slash"></i></button>
         @endif
           <button class="btn btn-sm" type="button" onClick="editProduct({{$product->id}})"><i class="fa fa-edit"></i></button>
           <button class="btn btn-sm" type="button" onClick="DeleteProduct({{$product->id}})"><i class="fas fa-trash-alt"></i></button>
         </td>
         <td>{{$product->code}}</td>
       </tr>
      @endforeach
   </tbody>
 </table>
 @endif
 <script>
 $(document).ready(function() {
     $('#example').DataTable();
 } );
 </script>
