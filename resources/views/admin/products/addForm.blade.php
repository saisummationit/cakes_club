<style>
.card-header{
  background-color: #e9ecef;
  padding: 0.5rem;
  font-size: 14px;
}
.card-body{
  padding: 0.75rem;
  font-size: 14px;
}
</style>
<br>
<h4 class="title-heading">New Product</h4>
<br>
<form method="post" id="newProductForm" action="{{route('products.save')}}" enctype="multipart/form-data">
  <div class="row">
    <div class="col-2"></div>
    <div class="col-8">
      <div class="card">
        <div class="card-header">Basic Details</div>
        <div class="card-body">
          <label>Product Name</label>
          <input type="text" class="form-control" name="productName" placeholder="Product Name">
          <br>
          <label>Product Code</label>
          <input type="text" class="form-control" name="code" placeholder="Product Code">
          <br>
          <label>In Stock</label>
          <input type="text" class="form-control col-3" name="stock">
          <br>
          <label>Price</label>
          <input type="text" class="form-control col-3" name="price">
        </div>
      </div>
      <br>
      <div class="card">
        <div class="card-header">Choose Category & Related Products</div>
        <div class="card-body">
          <input type="text" class="form-control" name="category" placeholder="" readonly id="selectedCategory" data-target="">
          <input type="hidden" name="categoryId" id="categoryId"><br>
          <button type="button" class="btn btn-info btn-sm float-right" onclick="selectCategory()">Choose Category</button><br><br>
          <input type="text" class="form-control" id="relatedprd" name="relatedprd" placeholder="" readonly><br>
          <input type="hidden" name="realatedProducts_id" id="relatedprdList"><br>
          <button type="button" class="btn btn-info btn-sm float-right" onclick="selectProduct()">Add Related Products</button>
        </div>
      </div>
      <br>
      <div class="card">
        <div class="card-header">Description</div>
        <div class="card-body">
          <textarea class="form-control" name="description" rows="7" id="summernote"></textarea>
        </div>
      </div>
      <br>
      <div class="card">
        <div class="card-header">Images</div>
          <div class="card-body">
              <div class="row">
              <div class="col-sm-4">
              <div id="wrapper">
                  <h4 class="img-heading">Default</h4>
                  <img id="output_image_default" height="150" width="150" src="/storage/banners/defaultImage.png"/><br><br>
                  <input type="file" name="defaultFile" accept="image/*" onchange="preview_image(event)">
              </div>
            </div>
            <div class="col-sm-8">
              <h4 class="img-heading">All Images</h4>
            <div id="wrapper_id">              
                <input type="file" accept="image/*" id="upload_file"  name="upload_files[]" multiple>
            </div>
          </div>
        </div>
        </div>
      </div>
      <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
      <br>
      <button class="btn btn-primary btn-sm btn-save float-right" type="submit"><i class="fa fa-check"></i>&nbsp;Save</button>
      <a href="{{route('products.index')}}" class="btn btn-danger btn-sm btn-cancel float-right"><i class="fa fa-times"></i>&nbsp;cancel</a>&nbsp;&nbsp;
      <br><br>
    </div>
    <div class="col-2"></div>
  </div>
</form>

<script>
$(document).ready(function() {
        $('#summernote').summernote({
          height: 150
        });
    });
    function selectCategory(){
    $.ajax({
        type:'get',
        url:'{{route("categories.getCategories")}}',
        success:function(data){
          $("#category-model-body").html(data);
          $("#selectCategoryModal").modal('show');
        },
        error:function(data){
          var message = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error!</strong> Something went wrong, please try again.</div>';
          $(".alert-messages").html(message);
        }
      });
    }

    function selectProduct(){
      debugger;
    $.ajax({
        type:'get',
        url:'{{route("products.getProducts")}}',
        success:function(data){
          $("#products-model-body").html(data);
          $("#selectProductsModal").modal('show');
        },
        error:function(data){
          var message = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error!</strong> Something went wrong, please try again.</div>';
          $(".alert-messages").html(message);
        }
      });
    }
    function previewList_image(event)
      {
        debugger;
        var total_file=document.getElementById("upload_file").files.length;
        for(var i=0;i<total_file;i++)
        {
          $('#wrapper_id').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'><br>");
        }
      }

</script>
