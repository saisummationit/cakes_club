
@if($product)
@php
$myString = $product->relatedproducts;
$myArray = explode(',', $myString);
@endphp
<ul id="treeview">
  @if(count($products) == 0)
    <center><h3>No Products added</h3></center>
  @endif
@if(count($products) > 0)
  @foreach($products as $prd)
      @if(in_array($prd->id,$myArray))
        <input type="checkbox" value="{{$prd->id}}" name="product" checked>&nbsp;<label>{{$prd->name}}</label><br>
      @else
        <input type="checkbox" value="{{$prd->id}}" name="product">&nbsp;<label>{{$prd->name}}</label><br>
      @endif
  @endforeach
@endif
  <button class="btn btn-primary btn-sm btn-save float-right" type="button" onclick="addProductsList()"><i class="fa fa-check"></i>&nbsp;Select</button>
</ul>
@else
@if(count($products) > 0)
   @foreach($products as $prd)
     <input type="checkbox" value="{{$prd->id}}" name="product">&nbsp;<label>{{$prd->name}}</label><br>
   @endforeach
     <button class="btn btn-primary btn-sm btn-save float-right" type="button" onclick="addProductsList()"><i class="fa fa-check"></i>&nbsp;Select</button>
   @else
     <center><h3>No Products added</h3></center>
   @endif
@endif
