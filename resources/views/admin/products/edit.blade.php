<br>
<h3 class="title-heading">Update Product</h3>
<br>
<form method="post" id="updateProductForm" action="{{route('products.update')}}" enctype="multipart/form-data">
  <div class="row">
    <div class="col-2"></div>
    <div class="col-8">
      <div class="card">
        <div class="card-header">Basic Details</div>
        <div class="card-body">
          <input hidden name="id" value="{{$product->id}}"/>
          <input hidden name="isActive" value="{{$product->isActive}}"/>
          <input hidden name="isAvailable" value="{{$product->isAvailable}}"/>
          <input hidden name="makeView" value="{{$product->makeView}}"/>
          <label>Product Name</label>
          <input type="text" class="form-control" name="productName" placeholder="Product Name" value="{{$product->name}}">
          <br>
          <label>Product Code</label>
          <input type="text" class="form-control" name="code" placeholder="Product Code" value="{{$product->code}}">
          <br>
          <label>In Stock</label>
          <input type="text" class="form-control col-3" name="stock" value="{{$product->stock}}">
          <br>
          <label>Price</label>
          <input type="text" class="form-control col-3" name="price" value="{{$product->price}}">
        </div>
      </div>
      <br>
      <div class="card">
        <div class="card-header">Choose Category & Related Products</div>
        <div class="card-body">
          <input type="text" class="form-control" name="category" placeholder="" readonly id="selectedCategory" data-text="{{$product->category->name}}" value="{{$product->category->name}}">
          <input type="hidden" name="categoryId" id="categoryId" value="{{$product->category->id}}"><br>
          <button type="button" class="btn btn-info btn-sm float-right" onclick="selectCategory()">Choose Category</button><br><br>
          @if($resultArray)
              <input type="text" class="form-control" id="relatedprd" name="relatedprd" placeholder="" value="{{$resultArray}}" readonly>
          @else
          <input type="text" class="form-control" id="relatedprd" name="relatedprd" placeholder="" value="" readonly>
          @endif
          <input type="hidden" name="realatedProducts_id" id="relatedprdList" value="{{$product->relatedproducts}}"><br>
          <button type="button" class="btn btn-info btn-sm float-right" onclick="selectProduct({{$product->id}})">Add Related Products</button>
        </div>
      </div>
      <br>
      <div class="card">
        <div class="card-header">Description</div>
        <div class="card-body">
          <textarea class="form-control" name="description" rows="7" id="summernote">{{$product->description}}</textarea>
        </div>
      </div>
      <br>
      <div class="card">
        <div class="card-header">Images</div>
          <div class="card-body">
              <div class="row">
              <div class="col-sm-4">
              <div id="wrapper">
                  <h4 class="img-heading">Default</h4>
                  <img id="output_image_default" height="150" width="150" src="/storage/products/{{$product->image_path}}"/><br><br>
                  <input type="file" name="defaultFile" accept="image/*" onchange="preview_image(event)">
              </div>
            </div>
            <div class="col-sm-8">
            <div id="wrapper">
                <h4 class="img-heading">All Images</h4>
                @if(count($relatedImages) > 0)
                @foreach($relatedImages as $image)
                <img id="output_image" height="75" width="75" src="/storage/products/{{$image->image_path}}" style="margin-top:2px;"/>
                @endforeach
                @endif
                <br><br>
                <input type="file" accept="image/*" id="upload_file"  name="upload_files[]" multiple>
            </div>
          </div>
        </div>
        </div>
      </div>
      <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
      <br>
      <button class="btn btn-primary btn-sm btn-save float-right" type="submit"><i class="fa fa-check"></i>&nbsp;Save</button>
      <a href="{{route('products.index')}}" class="btn btn-danger btn-sm btn-cancel float-right"><i class="fa fa-times"></i>&nbsp;cancel</a>&nbsp;&nbsp;
      <br><br>
    </div>
    <div class="col-2"></div>
  </div>
</form>

<script>
$(document).ready(function() {
        $('#summernote').summernote({
          height: 150
        });
    });
  function selectCategory(){
    $.ajax({
      type:'get',
      url:'{{route("categories.getCategories")}}',
      success:function(data){
        $("#category-model-body").html(data);
        $("#selectCategoryModal").modal('show');
      },
      error:function(data){
        var message = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error!</strong> Something went wrong, please try again.</div>';
        $(".alert-messages").html(message);
      }
    });
  }
  function selectProduct(id){
    debugger;
  $.ajax({
      type:'get',
      url:'{{route("products.getProducts")}}',
      data:{'id':id},
      success:function(data){
        $("#products-model-body").html(data);
        $("#selectProductsModal").modal('show');
      },
      error:function(data){
        var message = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error!</strong> Something went wrong, please try again.</div>';
        $(".alert-messages").html(message);
      }
    });
  }
</script>
