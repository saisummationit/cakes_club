<ul id="treeview">
  @foreach($parent_categories as $parent)
  <li><b data-target="{{$parent->id}}" data-level="{{$parent->parent_id}}" data-text="{{$parent->name}}">{{$parent->name}}</b>
      <ul>
          @foreach($parent->children as $children)
          <li><b data-target="{{$children->id}}" data-level="{{$children->parent_id}}" data-text="{{$children->name}}">{{$children->name}}</b></li>
          <ul>
            @foreach($children->children as $child)
              <li data-expanded="false"><b data-target="{{$child->id}}" data-level="{{$child->parent_id}}" data-text="{{$child->name}}">{{$child->name}}</b></li>
            @endforeach
          </ul>
          @endforeach
      </ul>
  </li>
  @endforeach
</ul>
<button class="btn btn-primary btn-sm btn-save float-right" type="button" id="btnSelectCategory"><i class="fa fa-check"></i>&nbsp;Select</button>

<script>
  $(document).ready(function(){
    $("#treeview").shieldTreeView();
  });
</script>
