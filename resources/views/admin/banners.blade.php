@extends('admin.master')
@section('content')
<div class="container bannersView" style="margin-top:40px;">
<div class="alert-messages">

</div>
<h3 class="title-heading">Banners</h3>
<br>
<button class="btn btn-primary btn-sm" onclick="AddBannerForm()"><i class="fa fa-plus-circle"></i>&nbsp;Add</button>
<br><br>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Active</th>
            <th>Default</th>
            <th></th>
        </tr>
    </thead>
    <tbody id="banners-body">
        @foreach($banners as $banner)
        <tr>
            <td>{{$banner->name}}</td>
            <td>{{$banner->description}}</td>
            <td class="text-center">
              @if($banner->isActive == 0)
              <a href="#" class="text-success" data-toggle="tooltip" title="Show Image in Banner" onclick="MakeActive(1,{{$banner->id}})"><i class="fa fa-check fa-sm"></i></a>
              @else
              <a href="#" class="text-danger" data-toggle="tooltip" title="Show Image in Banner" onclick="MakeActive(0,{{$banner->id}})"><i class="fa fa-times fa-sm"></i></a>
              @endif
            </td>
            <td class="text-center">
              @if($banner->default == 0)
              <label class="text-success">First Banner</label>
              @else
              <a href="#" class="text-danger" data-toggle="tooltip" title="Make this as first banner" onclick="MakeDefault(0,{{$banner->id}})"><i class="fa fa-times fa-sm"></i></a>
              @endif
            </td>
            <td class="text-center">
                <a href="#" data-toggle="tooltip" title="Edit" onclick="EditBanner({{$banner->id}})"><i class="fa fa-edit fa-sm"></i></a>&nbsp;
                <a href="#" data-toggle="tooltip" title="View" onclick="View({{$banner->id}})"><i class="fa fa-eye fa-sm"></i></a>&nbsp;
                <a href="#" data-toggle="tooltip" title="Delete" onclick="Delete({{$banner->id}},{{$banner->default}})"><i class="fa fa-edit fa-trash"></i></a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
</div>

@endsection
