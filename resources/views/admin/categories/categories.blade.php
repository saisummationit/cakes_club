<ul id="treeview">
  @foreach($parent_categories as $parent)
  <li><b data-target="{{$parent->id}}" data-level="{{$parent->parent_id}}">{{$parent->name}}</b>
      <ul>
          @foreach($parent->children as $children)
          <li><b data-target="{{$children->id}}" data-level="{{$children->parent_id}}">{{$children->name}}</b></li>
          <ul>
            @foreach($children->children as $child)
              <li data-expanded="false"><b data-target="{{$child->id}}" data-level="{{$child->parent_id}}">{{$child->name}}</b></li>
            @endforeach
          </ul>
          @endforeach
      </ul>
  </li>
  @endforeach
</ul>

<script>
  $(document).ready(function(){
    $("#treeview").shieldTreeView();
  });
</script>
