@extends('admin.master')
@section('content')
<div class="container bannersView" style="margin-top:40px;">
  <div class="alert-messages">

  </div>
  <h3 class="title-heading">Categories</h3>
  <br>
  <div class="row">
      <div class="container-fluid" id="catLevel1">
        <div class="col-md-6">
            <button href="#" style="text-decoration:none;" class="btn btn-primary btn-sm" onclick="AddCategory()"><i class="fa fa-plus-circle fa-sm"></i>&nbsp;Add</button>
            &nbsp;
            <button class="btn btn-primary btn-sm" id="subCategorybtn" onclick="AddSubCategory()" disabled><i class="fa fa-plus-circle fa-sm"></i>&nbsp;Add Sub Catgeory</button>
            &nbsp;
            <button  class="btn btn-info btn-sm" id="editbtn" disabled onclick="EditCategory()"><i class="fa fa-edit fa-sm"></i>&nbsp;Edit</button>
            &nbsp;
            <button  class="btn btn-danger btn-sm" id="deletebtn" disabled onclick="DeleteCategory()"><i class="fa fa-times fa-sm"></i>&nbsp;Delete</button>
            <br><br>
            <div class="treeViewPlugin">
              <ul id="treeview">
                @foreach($parent_categories as $parent)
                <li data-expanded="false"><b data-target="{{$parent->id}}" data-level="{{$parent->parent_id}}">{{$parent->name}}</b>
                    <ul>
                        @foreach($parent->children as $children)
                        <li data-hasChildren="true"><b data-target="{{$children->id}}" data-level="{{$children->parent_id}}">{{$children->name}}</b></li>
                        <ul>
                          @foreach($children->children as $child)
                            <li ><b data-target="{{$child->id}}" data-level="{{$child->parent_id}}">{{$child->name}}</b></li>
                          @endforeach
                        </ul>
                        @endforeach
                    </ul>
                </li>
                @endforeach
              </ul>
          </div>
        </div>
      </div>
  </div>
  <!-- Add Category Modal -->
  <div class="modal" id="AddCategoryModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4>Add Category</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form method="post" id="newCategoryForm">
            <label>Name: </label>
            <input type="text" class="form-control" placeholder="Name" name="name">
            <input hidden type="text" name="level" id="txtLevel">
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <br>
            <button type="submit" class="btn btn-primary btn-save float-right btn-sm">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- Edit Category Modal -->
  <div class="modal" id="EditCategoryModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4>Edit Category</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form method="post" id="editCategoryForm">
            <label>Name: </label>
            <input type="text" class="form-control" placeholder="Name" name="name" id="catName">
            <input hidden type="text" name="id" id="id">
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <br>
            <button type="submit" class="btn btn-primary btn-save float-right btn-sm">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script>

  function AddCategory(){
    var level = $(".sui-treeview-item-selected").find("b").data('level');
    if(level == undefined){
      level = 0;
    }
    $("#txtLevel").val(level);
    $("#AddCategoryModal").modal('show');
  }

  function AddSubCategory(){
    var level = $(".sui-treeview-item-selected").find("b").data('level');
    if(level == 0){
      level = $(".sui-treeview-item-selected").find("b").data('target');
    }
    $("#txtLevel").val(level);
    $("#AddCategoryModal").modal('show');
  }

  function EditCategory(){
    var id = $(".sui-treeview-item-selected").find("b").data('target');
    $("#id").val(id);
    $.ajax({
      type:'get',
      url:'{{route("categories.edit")}}',
      data:{'id':id},
      success:function(data){
        $("#catName").val(data);
        $("#EditCategoryModal").modal('show');
      },
      error:function(data){
        alert(data.responseText);
      }
    });
  }

  function DeleteCategory(){
    var id = $(".sui-treeview-item-selected").find("b").data('target');
    var deleted = confirm("If you try to delete this categories, sub categories and products under this category will be deleted. Are you sure to delete?.");
    if(!deleted){
      return false;
    }
    $.ajax({
      type:'get',
      url:'{{route("categories.delete")}}',
      data:{'id':id},
      success:function(data){
        $("#subCategorybtn").attr('disabled',true);
        $("#editbtn").attr('disabled',true);
        $("#deletebtn").attr('disabled',true);
        $(".treeViewPlugin").html(data);
      },
      error:function(data){
        alert(data.responseText);
      }
    });
  }

  $(document).ready(function(){

    $("#treeview").shieldTreeView();

    $(document).on("click",".sui-treeview-item-selected",function(){
      if($(".sui-treeview-item-selected").find("b").data('level') == 0){
          $("#subCategorybtn").removeAttr('disabled');
      }else{
          $("#subCategorybtn").attr('disabled',true);
      }
      $("#editbtn").removeAttr('disabled');
      $("#deletebtn").removeAttr('disabled');
    });

    $(document).on("submit","form#newCategoryForm",function(){
      $.ajax({
        type:'post',
        url:'{{route("categories.save")}}',
        data:$(this).serialize(),
        success:function(data){
          $("#AddCategoryModal").modal('hide');
          $("#subCategorybtn").attr('disabled',true);
          $("#editbtn").attr('disabled',true);
          $("#deletebtn").attr('disabled',true);
          $(".treeViewPlugin").html(data);
        },
        error:function(data){
          alert(data.responseText);
        }
      });
      return false;
    });

    $(document).on("submit","form#editCategoryForm",function(){
      $.ajax({
        type:'post',
        url:'{{route("categories.update")}}',
        data:$(this).serialize(),
        success:function(data){
          $("#EditCategoryModal").modal('hide');
          $("#subCategorybtn").attr('disabled',true);
          $("#editbtn").attr('disabled',true);
          $("#deletebtn").attr('disabled',true);
          $(".treeViewPlugin").html(data);
        },
        error:function(data){
          alert(data.responseText);
        }
      });
      return false;
    });

  });

</script>
@endsection
