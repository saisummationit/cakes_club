<h3 class="title-heading">New Banner</h3>
<br>
<form method="post" id="newBannerForm" enctype="multipart/form-data" action="/admin/banner/save">
    <label>Name:</label>
    <input type="text" class="form-control" name="name">
    <br>
    <label>Description:</label>
    <textarea class="form-control" name="description" rows="5"></textarea>
    <br>
    <label>Image:</label>
    <input type="file" class="form-control" name="banner">
    <br>
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
    <input type="submit" class="btn btn-success btn-sm float-right btn-save" value="Save">
    <a href="{{route('banner.index')}}" class="btn btn-danger btn-sm float-right btn-cancel" style="margin-right:10px;">Cancel</a>
</form>
