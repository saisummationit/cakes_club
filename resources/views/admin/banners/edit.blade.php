<h3 class="title-heading">Update Banner</h3>
<br>
<form method="post" id="updateBannerForm" enctype="multipart/form-data" action="/admin/banner/update">
    <label>Name:</label>
    <input type="text" class="form-control" name="name" value="{{$banner->name}}">
    <input hidden name="id" value="{{$banner->id}}"/>
    <br>
    <label>Description:</label>
    <textarea class="form-control" name="description" rows="5">{{$banner->description}}</textarea>
    <br>
    <label>Image:</label>
    <input type="file" class="form-control" name="banner">
    <br>
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
    <input type="submit" class="btn btn-success btn-sm float-right btn-save" value="Update">
    <a href="{{route('banner.index')}}" class="btn btn-danger btn-sm float-right btn-cancel" style="margin-right:10px;">Cancel</a>
    <br>
</form>
