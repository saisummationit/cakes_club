<a href="{{route('banner.index')}}" style="text-decoration:none;"><i class="fa fa-home"></i>&nbsp;Back to Banners</a>
<br>
<br>
<h3 class="title-heading">{{$banner->name}}</h3>
<br>
<div class="container-fluid">
  <label>Description:</label>
  <p>{{$banner->description}}</p>
  <br>
  <img class="image img-responsive" src="{{asset('storage/banners/'.$banner->imageName)}}" style="height:400px;width:100%;"/>
</div>
