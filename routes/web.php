<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function(){
    Route::get('/dashboard',[
        'uses' => 'Admin\DashboardController@index',
        'as' => 'dashboard.index'
    ]);
    Route::get('/banner',[
        'uses' => 'Admin\BannerController@index',
        'as' => 'banner.index'
    ]);
    Route::get('/banner/add',[
        'uses' => 'Admin\BannerController@addForm',
        'as' => 'banner.addForm'
    ]);
    Route::post('/banner/save',[
        'uses'=>'Admin\BannerController@save',
        'as'=>'banner.create'
    ]);
    Route::get('/banner/changeStatus',[
        'uses'=>'Admin\BannerController@changeStatus',
        'as'=>'banner.changeStatus'
    ]);
    Route::get('/banner/changeDefault',[
        'uses'=>'Admin\BannerController@changeDefault',
        'as'=>'banner.changeDefault'
    ]);
    Route::get('/banner/delete',[
        'uses'=>'Admin\BannerController@delete',
        'as'=>'banner.delete'
    ]);
    Route::get('/banner/view',[
        'uses'=>'Admin\BannerController@view',
        'as'=>'banner.view'
    ]);
    Route::get('/banner/edit',[
        'uses' => 'Admin\BannerController@editForm',
        'as' => 'banner.editForm'
    ]);
    Route::post('/banner/update',[
        'uses'=>'Admin\BannerController@update',
        'as'=>'banner.update'
    ]);
});

Route::group(['prefix'=>'admin/category','middleware'=>['auth']],function(){
  Route::get('/',[
    'uses'=>'Admin\CategoriesController@index',
    'as'=>'categories.index'
  ]);
  Route::post('/save',[
    'uses'=>'Admin\CategoriesController@save',
    'as'=>'categories.save'
  ]);
  Route::get('/edit',[
    'uses'=>'Admin\CategoriesController@edit',
    'as'=>'categories.edit'
  ]);
  Route::post('/update',[
    'uses'=>'Admin\CategoriesController@update',
    'as'=>'categories.update'
  ]);
  Route::get('/delete',[
    'uses'=>'Admin\CategoriesController@delete',
    'as'=>'categories.delete'
  ]);
  Route::get('/getCategories',[
    'uses'=>'Admin\CategoriesController@getCategories',
    'as'=>'categories.getCategories'
  ]);


});

Route::group(['prefix'=>'admin/product','middleware'=>['auth']],function(){
  Route::get('/',[
    'uses'=>'Admin\ProductController@index',
    'as'=>'products.index'
  ]);
  Route::get('/add',[
    'uses'=>'Admin\ProductController@addForm',
    'as'=>'products.addForm'
  ]);
  Route::post('/save',[
    'uses'=>'Admin\ProductController@save',
    'as'=>'products.save'
  ]);
  Route::post('/update',[
    'uses'=>'Admin\ProductController@update',
    'as'=>'products.update'
  ]);
  Route::get('/delete',[
    'uses'=>'Admin\ProductController@delete',
    'as'=>'products.delete'
  ]);
  Route::get('/edit',[
    'uses'=>'Admin\ProductController@editForm',
    'as'=>'products.editForm'
  ]);
  Route::get('/toBeViewOnCustomer',[
    'uses'=>'Admin\ProductController@toBeViewOnCustomer',
    'as'=>'products.toBeViewOnCustomer'
  ]);
  Route::get('/getProducts',[
    'uses'=>'Admin\ProductController@getProducts',
    'as'=>'products.getProducts'
  ]);
  Route::get('/addProductList',[
    'uses'=>'Admin\ProductController@addProductList',
    'as'=>'products.addProductList'
  ]);


});

//---------------------------------------------- client routes--------------------------------------------------
Route::get('/cakes', [
  'uses'=>'Client\CakesController@index',
  'as'=>'cakes.index'
]);
Route::get('/products', function () {
  return view('client.products');
});
Route::get('/getProductDetails/{id}', [
  'uses'=>'Client\CakesController@getProductDetails',
  'as'=>'Cakes.getProductDetails'
]);

Route::get('/checkout', function () {
  return view('client.checkout');
});
Route::get('/cart', function () {
  return view('client.cart');
});
//add to cart route

Route::post('/addtoCart/{id}', [
  'uses'=>'Client\CartController@addtoCart',
  'as'=>'Cakes.addtoCart'
]);
  
