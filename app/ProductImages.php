<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImages extends Model
{
    protected $table = 'productimages';
    public function product(){
      return $this->hasMany('App\ProductImages','product_id');
    }
}
