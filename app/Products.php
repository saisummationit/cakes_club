<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    public function category(){
      return $this->belongsTo('App\Categories','category_id');
    }

    public function images(){
      return $this->belongsTo('App\ProductImages','product_id');
    }
}
