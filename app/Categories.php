<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
      public function children(){
          return $this->hasMany('App\Categories','parent_id','id')->with('children');
      }

      public function parent(){
          return $this->belongsTo('App\categories','parent_id');
      }

      public function product(){
        return $this->hasMany('App\Categories','category_id');
      }
}
