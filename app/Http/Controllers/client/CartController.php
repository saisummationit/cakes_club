<?php

namespace App\Http\Controllers\client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Products;
use App\ProductImages;
use App\Cart;

class CartController extends Controller
{
    public function addtoCart(Request $request,$id){
        $cart = new Cart;
        $cart->product_id = $request->get('id');
        $cart->quantity=1;
        $cart->customer_id=1;
        $cart->save();
    }
        
}
