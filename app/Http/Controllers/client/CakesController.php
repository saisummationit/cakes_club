<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Banners;
use App\Products;
use App\ProductImages;
class CakesController extends Controller
{
    public function index(){
        $banners = Banners::where('isActive','=',0)->orderBy('default', 'asc')->get();
        $products = Products::with('category')->where('isActive','=','0')->get();
        return view('client.products',compact('banners','products'));

    }
public function getProductDetails($id){
  
    $product = Products::with('category','category')->find($id);
       $resultArray = array();
       $prdarr = $product->relatedproducts;
       if($prdarr != ""){
       $myArray = explode(',', $prdarr);
       foreach($myArray as $arr){
            $products = Products::find($arr);
            if($products->isActive == 0){
            $relatedProducts[] = $products;
            
          }
         
        }
    //    $resultArray = implode (", ", $resultArray);
      }
        $relatedImages = ProductImages::where('product_id','=', $id)->get();
        return view('client.product-details',compact('product','relatedProducts','relatedImages'));
}


}
