<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Products;
use App\ProductImages;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function index(){
      $products = Products::with('category')->where('isActive','=','0')->get();
      return view('admin.products.index',compact('products'));
    }

    public function addForm(){
      return view('admin.products.addForm');
    }

    public function save(Request $request){
      $product = new Products;
      $product->name = $request->get('productName');
      $product->stock = $request->get('stock');
      $product->code = $request->get('code');
      $product->price = $request->get('price');
      $product->description = $request->get('description');
      $product->category_id = $request->get('categoryId');
      $product->makeView = 0;
      $product->isAvailable = 0;
      $product->isActive = 0;
      $product->relatedproducts = $request->get('realatedProducts_id');
      if($request->file('defaultFile')){
        $file = $request->file('defaultFile');
        $extension = $file->getClientOriginalExtension();
        Storage::disk('products')->put($file->getFilename().'.'.$extension,  File::get($file));
        $product->image_name = $file->getClientOriginalName();
        $product->image_path = $file->getFilename().'.'.$extension;
      }
      $product->save();
      if($product->id){
        $relatedImages = $request->file('upload_files');
        if($relatedImages){
        foreach($relatedImages as $prd){ 
              $productImages = new ProductImages;
              $productImages->product_id = $product->id;
              $extension = $prd->getClientOriginalExtension();
              Storage::disk('products')->put($prd->getFilename().'.'.$extension,  File::get($prd));
              $productImages->image_name = $prd->getClientOriginalName();
              $productImages->image_path = $prd->getFilename().'.'.$extension;
              $productImages->save();
            }
          }
        }
        return redirect()->route('products.index');
    }
    public function delete(Request $request){
      $product = Products::find($request->get('id'));
      if($product){
        $product->isActive = 1;
        $product->save();
        $products = Products::with('category')->where('isActive','=','0')->get();
        return view('admin.products.products',compact('products'));
       }
      else{
        return 0;
       }
     }
      public function editForm(Request $request){
       $product = Products::with('category','category')->find($request->get('id'));
       $resultArray = array();
       $prdarr = $product->relatedproducts;
       if($prdarr != ""){
       $myArray = explode(',', $prdarr);
       foreach($myArray as $arr){
            $products = Products::find($arr);
            if($products->isActive == 0){
            $resultArray[] = $products->name;
          }
        }
       $resultArray = implode (", ", $resultArray);
      }
        $relatedImages = ProductImages::where('product_id','=', $request->get('id'))->get();
        return view('admin.products.edit',compact('product','resultArray','relatedImages'));
      }
      public function update(Request $request){
        $product = Products::find($request->get('id'));
        if($product){
          $product->name = $request->get('productName');
          $product->stock = $request->get('stock');
          $product->code = $request->get('code');
          $product->price = $request->get('price');
          $product->description = $request->get('description');
          $product->category_id = $request->get('categoryId');
          $product->relatedproducts = $request->get('realatedProducts_id');
          $product->makeView = $request->get('makeView');;
          $product->isAvailable = $request->get('isAvailable');
          $product->isActive = $request->get('isActive');
          if($request->file('defaultFile')){
            $file = $request->file('defaultFile');
            $extension = $file->getClientOriginalExtension();
            Storage::disk('products')->put($file->getFilename().'.'.$extension,  File::get($file));
            $product->image_name = $file->getClientOriginalName();
            $product->image_path = $file->getFilename().'.'.$extension;
          }
          $product->save();
            $relatedImages = $request->file('upload_files');
            if($relatedImages){
            foreach($relatedImages as $prd){
              $productImages = new ProductImages;
              $productImages->product_id = $product->id;
              $extension = $prd->getClientOriginalExtension();
              Storage::disk('products')->put($prd->getFilename().'.'.$extension,  File::get($prd));
              $productImages->image_name = $prd->getClientOriginalName();
              $productImages->image_path = $prd->getFilename().'.'.$extension;
              $productImages->save();
          }
        }
        return redirect()->route('products.index');
      }
    }
      public function toBeViewOnCustomer(Request $request){
        $view = $request->get('view');
        $id = $request->get('id');
        $product = Products::find($id);
        if($product){
          $product->makeView = $view;
          $product->save();
        }
        $products = Products::with('category')->where('isActive','=','0')->get();
        return view('admin.products.products',compact('products'));
      }
    public function getProducts(Request $request){
        if($request->get('id')){
            $product = Products::with('category','category')->find($request->get('id'));
            $products = Products::with('category')->whereNotIn('id',array($request->get('id')))->where('isActive','=','0')->get();
        }
        else{
          $product = "";
          $products = Products::with('category')->where('isActive','=','0')->get();
        }
        return view('admin.products.productsList',compact('products','product'));
      }
}
