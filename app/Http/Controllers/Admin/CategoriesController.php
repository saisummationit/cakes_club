<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categories;

class CategoriesController extends Controller
{
    public function index(){
      $parent_categories = Categories::with('children')->where('parent_id','=',0)->get();
      return view('admin.categories.index',compact('parent_categories'));
    }

    public function save(Request $request){
      $category = new Categories;
      $category->name = $request->get('name');
      $category->parent_id = $request->get('level');
      $category->save();
      $parent_categories = Categories::where('parent_id','=',0)->get();
      return view('admin.categories.categories',compact('parent_categories'));
    }

    public function edit(Request $request){
      $category = Categories::find($request->get('id'));
      return $category->name;
    }

    public function update(Request $request){
      $category = Categories::find($request->get('id'));
      $category->name = $request->get('name');
      $category->save();
      $parent_categories = Categories::where('parent_id','=',0)->get();
      return view('admin.categories.categories',compact('parent_categories'));
    }

    public function delete(Request $request){
      $category = Categories::find($request->get('id'));
      $category->delete();
      $collection = Categories::where('parent_id', $request->get('id'))->get(['id']);
      Categories::destroy($collection->toArray());
      $parent_categories = Categories::where('parent_id','=',0)->get();
      return view('admin.categories.categories',compact('parent_categories'));
    }
    public function getCategories(){
      $parent_categories = Categories::where('parent_id','=',0)->get();
      return view('admin.products.categories',compact('parent_categories'));
    }
}
