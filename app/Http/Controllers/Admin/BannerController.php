<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Banners;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;

class BannerController extends Controller
{
    public function index(){
        $banners = Banners::all();
        return view('admin.banners',compact('banners'));
    }

    public function addForm(){
        return view('admin.banners.add');
    }

    public function save(Request $request){
        $banner = new Banners;
        $banner->name = $request->get('name');
        $banner->description = $request->get('description');
        if($request->file('banner')){
          $file = $request->file('banner');
          $extension = $file->getClientOriginalExtension();
          Storage::disk('public')->put($file->getFilename().'.'.$extension,  File::get($file));
          $banner->imagePath = $file->getClientOriginalName();
          $banner->imageName = $file->getFilename().'.'.$extension;
        }
        $banner->isActive = 0;
        $bannersPresent = Banners::where('isActive','=',0)->first();
        if(!$bannersPresent)
          $banner->default = 0;
        else
          $banner->default = 1;
        $banner->save();
        return redirect()->route('banner.index');
    }

    public function changeStatus(Request $request){
      $status = $request->get('status');
      $id = $request->get('id');
      $banner = Banners::find($id);
      if($banner){
        $banner->isActive = $status;
      }
      $banner->save();
      $banners = Banners::all();
      return view('admin.banners.banners',compact('banners'));
    }

    public function changeDefault(Request $request){
      $status = $request->get('status');
      $id = $request->get('id');
      $updated = DB::table('banners')->update(['default'=>1]);
      if($updated){
        $banner = Banners::find($id);
        if($banner){
          $banner->default = $status;
        }
        $banner->save();
        $banners = Banners::all();
        return view('admin.banners.banners',compact('banners'));
      }else{
        return 1;
      }
    }

    public function delete(Request $request){
      $banner = Banners::find($request->get('id'));
      if($banner){
        if($banner->default == 0){
          $maxId = Banners::max('id');
          if($maxId){
              $updateBanner = Banners::find($maxId);
              if($updateBanner){
                $updateBanner->default = 0;
                $updateBanner->save();
              }
          }
          $banner->delete();
        }
        $banners = Banners::all();
        return view('admin.banners.banners',compact('banners'));
      }else{
        return 0;
      }
    }

    public function view(Request $request){
      $banner = Banners::find($request->get('id'));
      return view('admin.banners.view',compact('banner'));
    }

    public function editForm(Request $request){
      $id = $request->get('id');
      $banner = Banners::find($id);
      return view('admin.banners.edit',compact('banner'));
    }

    public function update(Request $request){
      $banner = Banners::find($request->get('id'));
      if($banner){
        $banner->name = $request->get('name');
        $banner->description = $request->get('description');
        if($request->file('banner')){
          $file = $request->file('banner');
          $extension = $file->getClientOriginalExtension();
          Storage::disk('public')->put($file->getFilename().'.'.$extension,  File::get($file));
          $banner->imagePath = $file->getClientOriginalName();
          $banner->imageName = $file->getFilename().'.'.$extension;
        }
        $banner->save();
      }
      return redirect()->route('banner.index');
    }
}
