<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('code')->nullable();
            $table->integer('stock')->nullable();
            $table->string('price')->nullable();
            $table->text('description')->nullable();
            $table->integer('category_id');
            $table->string('realtedproducts')->nullable();
            $table->string('image_name')->nullable();
            $table->string('image_path')->nullable();
            $table->integer('isAvailable');
            $table->integer('makeView');
            $table->integer('isActive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
